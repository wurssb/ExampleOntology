#!/bin/bash
#============================================================================
#title          :Ontology API Builder
#description    :Builds an R and Java API from an OWL/SheX ontology
#author         :Jesse van Dam, Jasper Koehorst
#date           :2017
#version        :0.1
#============================================================================
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

if [ "$1" == "test" ]; then
	gradle build -b "$DIR/build.gradle" --info
else
	echo "Skipping tests, run './install.sh test' to perform tests"
	gradle build -b "$DIR/build.gradle" -x test
fi

cp $DIR/build/libs/*jar $DIR/

# A non zero exit is given
#java -jar $DIR/*.jar --help

# #!/bin/bash
# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# # Pull latest
# git -C "$DIR" pull

# gradle install -b "$DIR/build.gradle" 

# cp $DIR/build/libs/GBOLApi.jar $DIR/

# #Comment: Null pointer issue
# if [ -d $DIR/../RExampleOntology ]; then
#   cp $DIR/GBOLApi.jar $DIR/../RExampleOntology/inst/java/
# fi

