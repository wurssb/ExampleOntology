package org.empusa;

import java.util.HashMap;
import java.util.HashSet;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.lang3.StringUtils;

public class Main {
  
  private Domain domain ;
  private org.w3.owl.domain.Class root;
  public static void main(String args[]) throws Exception {
    new Main();
  }

  public Main() throws Exception {
    writeTypes();
  }

  private String parseValueSyntax(String in,String varName)
  {
    /*
     * 
PREFIX gbol: <http://gbol.life/0.1/>

SELECT ?syntax (count(?a) as ?count)
{
  ?a a ?b .
  FILTER EXISTS
  {
    ?a gbol:envPackage ?package .
  }
  ?a gbol:valueSyntax ?syntax .
  FILTER(!regex(?syntax,"^\\[.*\\]$"))
}
group by ?syntax
order by desc(?count)
     */
    if(in.equals("{boolean}"))
    {
      return "xsd:Boolean";
    }
    else if(in.equals("{term}"))
    {
      return "IRI";
    }
    else if(in.startsWith("[") && in.endsWith("]"))
    {
      //TODO create the type;
      return varName + "Type";
    }
    else if(in.equals("{integer}"))
    {
      return "xsd:Integer";
    }
    else if(in.equals("{PMID|DOI|URL}"))
    {
      return "@:Document";
    }
    else if(in.equals("{text}"))
    {
      return "xsd:String";
    }
    else if(in.equals("{float}"))
    {
      return "xsd:Float";
    }
    else if(in.equals("{timestamp}"))
    {
      return "xsd:dateTime";
    }
    else if(in.equals("{interval}"))
    {
      return "xsd:dateTime";
    }
    else if(in.startsWith("{float} {unit}"))
    {
      //TODO float unit
      return "Unit";
    }
    else if(in.startsWith("{text};{interval}"))
    {
      //TODO text interval
      return "Unit";
    }
    else if(in.startsWith("{text};{period}"))
    {
      //TODO text period
      return "Unit";
    }
    else if(in.startsWith("{term}; {timestamp}"))
    {
      //TODO term timestamp
      return "Unit";
    }
    else if(in.startsWith("{text}; {timestamp}"))
    {
      //TODO test timestamp
      return "Unit";
    }
    else if(in.startsWith("{text};{float} {unit}"))
    {
      //TODO text float unit
      return "Unit";
    }
    else if(in.equals("{float} m"))
    {
      //TODO float m
      return "Unit meter";
    }
    System.out.println(in);
    return null;
  }
  
  private String cleanName(String in)
  {
    in = in.replace(":","_");
    String parts[] = in.split("_");
    for(int i = 0;i < parts.length;i++)
    {
      parts[i] = StringUtils.capitalize(parts[i]);
    }
    in = StringUtils.join(parts);
    in = in.replace(' ','_');
    return in;
  }

  public void prepBIBO() throws Exception
  {
    RDFSimpleCon con = new RDFSimpleCon("file://designdocs/bibtex/bibtex.owl");
    HashMap<String,String> propDefs = new HashMap<String,String>();
    for(ResultLine item : con.runQuery("getProps.sparql",false))
    {
      String prop = item.getIRI("prop");
      String domain = item.getIRI("domain");
      String range = item.getIRI("range");
      System.out.println("" + prop + " : " + domain + " - " + range);
      String def = propDefs.get(domain);
      if(def == null)
        def = "";
      def += con.getShortForm(prop) + " ";
      if(range == null)
        def += "null";
      else if(range.equals("http://www.w3.org/2000/01/rdf-schema#Literal"))
        def += "xsd:String";
      else
        def += "@" + con.getShortForm(range);
      def += ";\n";
      propDefs.put(domain,def);
    }
    for(String domain : propDefs.keySet())
    {
      String propDef = propDefs.get(domain);
      con.addLit(domain,"http://ssb.wur.nl/OwlShexCodeGen#propertyDefinitions",propDef);
      System.out.println(domain + ":\n" + propDef + "\n");
    }
    con.save("./gbolBibo.ttl");
  }
  
  public void writeTypes() throws Exception
  {
    RDFSimpleCon con = new RDFSimpleCon("");
    domain = new Domain(con);
    root = domain.make(org.w3.owl.domain.Class.class,"http://gbol.life/0.1/Rank");
    String in = "SUPERKINGDOM,KINGDOM,SUBKINGDOM,SUPERPHYLUM,PHYLUM,SUBPHYLUM,SUPERCLASS,CLASS,SUBCLASS,INFRACLASS,SUPERORDER,ORDER,SUBORDER,INFRAORDER,PARVORDER,SUPERFAMILY,FAMILY,SUBFAMILY,TRIBE,SUBTRIBE,GENUS,SUBGENUS,SPECIES GROUP,SPECIES SUBGROUP,SPECIES,SUBSPECIES,VARIETAS,FORMA";
    String partWords[] = "SUPER,SUB,INFRA,PARV".toLowerCase().split(",");
    boolean deprecated = false;
    org.w3.owl.domain.Class parentRank = null;
    for(String item : in.split(","))
    {
      item = item.trim();
      //replace(":","_").replaceAll("\\)","").replaceAll("\\(","")
      String parts[] = item.toLowerCase().split(" ");
      String textName = "";
      for(int i = 0;i < parts.length;i++)
      {
        parts[i] = StringUtils.capitalize(parts[i]);
        boolean found = false;
        for(String partCheck : partWords)
        {
          if(parts[i].toLowerCase().startsWith(partCheck))
          {
            textName += parts[i].substring(0,partCheck.length()).toLowerCase() + " " + parts[i].substring(partCheck.length()) + " ";
            parts[i] = parts[i].substring(0,partCheck.length()) + StringUtils.capitalize(parts[i].substring(partCheck.length()));
            found = true;
          }
        }
        if(!found)
          textName += parts[i].toLowerCase() + " ";
      }
      textName = textName.substring(0,textName.length() - 1);
      String name = StringUtils.join(parts);
      //name = name.replace(' ','_');
      org.w3.owl.domain.Class node = domain.make(org.w3.owl.domain.Class.class,"http://gbol.life/0.1/Rank" + name);
      node.addSubClassOf(root);
      this.domain.getRDFSimpleCon().addLit(node.getResource(),"rdf:label",textName);
      this.domain.getRDFSimpleCon().addLit(node.getResource(),"rdf:definition","The taxonomic rank: " + textName);
      if(parentRank != null)
      {
        this.domain.getRDFSimpleCon().add(node.getResource(),"http://gbol.life/0.1/parentRank",parentRank.getResource());
      }
      parentRank = node;
      //if(deprecated)
      //  this.domain.getRDFSimpleCon().add(node.getResource(),"owl:deprecated",true);
    }
    int level = 1;
    for(String item : in.split(","))
    {
      org.w3.owl.domain.Class node = domain.make(org.w3.owl.domain.Class.class,"http://gbol.life/0.1/RankLevel" + level);
      node.addSubClassOf(root);
      this.domain.getRDFSimpleCon().addLit(node.getResource(),"rdf:label","Rank Level " + level);
      this.domain.getRDFSimpleCon().addLit(node.getResource(),"rdf:definition","Exact rank unknown: rank level " + level);
      if(parentRank != null)
      {
        this.domain.getRDFSimpleCon().add(node.getResource(),"http://gbol.life/0.1/parentRank",parentRank.getResource());
      }
      parentRank = node;
      level++;
      //if(deprecated)
      //  this.domain.getRDFSimpleCon().add(node.getResource(),"owl:deprecated",true);
    }
    con.save("temp.ttl");
  }
  
  public void writeAminoAcids() throws Exception
  {
    RDFSimpleCon con = new RDFSimpleCon("");
    domain = new Domain(con);
    root = domain.make(org.w3.owl.domain.Class.class,"http://gbol.life/0.1/AminoAcid");
    
    addAminoAcid("3-Hydroxyproline",null,"3Hyp");
    addAminoAcid("4-Aminobutyric acid (piperidinic acid)",null,"4Abu");
    addAminoAcid("4-Hydroxyproline",null,"4Hyp");
    addAminoAcid("2-Aminoadipic acid",null,"Aad");
    addAminoAcid("2-Aminobutyric acid",null,"Abu");
    addAminoAcid("6-Aminocaproic acid",null,"Acp");
    addAminoAcid("2-Aminoheptanoic acid",null,"Ahe");
    addAminoAcid("allo-Hydroxylysine",null,"aHyl");
    addAminoAcid("2-Aminoisobutyric acid",null,"Aib");
    addAminoAcid("allo-Isoleucine",null,"aIle");
    addAminoAcid("2-Aminopimelic acid",null,"Apm");
    addAminoAcid("3-Aminoadipic acid",null,"bAad");
    addAminoAcid("3-Aminoisobutyric acid",null,"bAib");
    addAminoAcid("beta-Alanine (beta-Aminoproprionic acid)",null,"bAla");
    addAminoAcid("2,4-Diaminobutyric acid",null,"Dbu");
    addAminoAcid("Desmosine",null,"Des");
    addAminoAcid("2,2-Diaminopimelic acid",null,"Dpm");
    addAminoAcid("2,3-Diaminoproprionic acid",null,"Dpr");
    addAminoAcid("N-Ethylasparagine",null,"EtAsn");
    addAminoAcid("N-Ethylglycine",null,"EtGly");
    addAminoAcid("Hydroxylysine",null,"Hyl");
    addAminoAcid("Isodesmosine",null,"Ide");
    addAminoAcid("N-Methylglycine (sarcosine)",null,"MeGly");
    addAminoAcid("N-Methylisoleucine",null,"MeIle");
    addAminoAcid("6-N-Methyllysine",null,"MeLys");
    addAminoAcid("N-Methylvaline",null,"MeVal");
    addAminoAcid("Norleucine",null,"Nle");
    addAminoAcid("Norvaline",null,"Nva");
    addAminoAcid("Ornithine",null,"Orn");
    addAminoAcid("Selenocysteine",'U',"Sec");
    addAminoAcid("Alanine",'A',"Ala");
    addAminoAcid("Arginine",'R',"Arg");
    addAminoAcid("Asparagine",'N',"Asn");
    addAminoAcid("Aspartic acid (Aspartate)",'D',"Asp");
    addAminoAcid("Asparagine or Aspartate",'B',"Asx");
    addAminoAcid("Cysteine",'C',"Cys");
    addAminoAcid("Glutamine",'Q',"Gln");
    addAminoAcid("Glutamic acid (Glutamate)",'E',"Glu");
    addAminoAcid("Glutamine or GLutamate",'Z',"Glx");
    addAminoAcid("Glycine",'G',"Gly");
    addAminoAcid("Histidine",'H',"His");
    addAminoAcid("Isoleucine",'I',"Ile");
    addAminoAcid("Leucine",'L',"Leu");
    addAminoAcid("Lysine",'K',"Lys");
    addAminoAcid("Methionine",'M',"Met");
    addAminoAcid("Other",'X',"OTHER");
    addAminoAcid("Phenylalanine",'F',"Phe");
    addAminoAcid("Proline",'P',"Pro");
    addAminoAcid("Pyrrolysine",'O',"Pyl");
    addAminoAcid("Serine",'S',"Ser");
    addAminoAcid("termination codon",'*',"TERM");
    addAminoAcid("Threonine",'T',"Thr");
    addAminoAcid("Tryptophan",'W',"Trp");
    addAminoAcid("Tyrosine",'Y',"Tyr");
    addAminoAcid("Valine",'V',"Val");
    addAminoAcid("Any amino acid",'X',"Xaa");
    addAminoAcid("Isoleucine or Leucine",'J',"Xle");
    con.save("temp.ttl");
  }
  
  public void addAminoAcid(String fullName,Character letter,String code)
  {
    org.w3.owl.domain.Class node = domain.make(org.w3.owl.domain.Class.class,"http://gbol.life/0.1/" + code);
    node.addSubClassOf(root);
    this.domain.getRDFSimpleCon().addLit(node.getResource(),"http://gbol.life/0.1/ddbjLabel",fullName);
    if(letter != null)
      this.domain.getRDFSimpleCon().addLit(node.getResource(),"http://gbol.life/0.1/aminoAcidLetter","" + letter);
  }
  
  public void moveQualLifiersUp() throws Exception {
    RDFSimpleCon con = new RDFSimpleCon("file://root-ontology.ttl");
    for (int i = 1; i < 7; i++) {
      System.out.println("update: " + i);
      con.runUpdateQuery("moveup1.sparql", StringUtils.repeat("/rdfs:subClassOf", i));
    }
    con.runUpdateQuery("moveup2.sparql");
    // con.save("gbol3.ttl");
  }
/*
  public void loadAnnotProps() throws Exception {
    RDFSimpleCon con = new RDFSimpleCon("file://gbol.ttl");
    Domain domain = new Domain(con);

    String qIn[] = FileUtils.readFileToString(new File("./datain/qualifiers.txt")).split("\n");
    String lastKey = null;
    String val = "";
    HashMap<String, String> lastQual = null;
    HashMap<String, HashMap<String, String>> qualifiers =
        new HashMap<String, HashMap<String, String>>();
    HashSet<String> allprops = new HashSet<String>();
    for (String line : qIn) {
      if (line.trim().equals(""))
        continue;
      if (line.startsWith(" ") || line.startsWith("\t")) {
        val += line.trim();
      } else {
        if (lastKey != null) {
          lastQual.put(lastKey, val);
        }
        lastKey = line.split("[ \t]")[0];
        val = line.substring(lastKey.length()).trim();
        if (lastKey.endsWith("s"))
          lastKey = lastKey.substring(0, lastKey.length() - 1);
        lastKey = lastKey.substring(0, 1).toLowerCase() + lastKey.substring(1);
        allprops.add(lastKey);
        if (lastKey.equals("qualifier")) {
          lastQual = new HashMap<String, String>();
          if (val.endsWith("="))
            val = val.substring(0, val.length() - 1);
          qualifiers.put(val.substring(1), lastQual);
          lastKey = null;
        }
      }
    }
    lastQual.put(lastKey, val);

    qIn = FileUtils.readFileToString(new File("./datain/parserQualifiers.txt")).split("\n");
    for (String line : qIn) {
      if (line.startsWith("#") || line.trim().equals(""))
        continue;
      String parts[] = line.split("\t");
      lastQual = qualifiers.get(parts[0]);
      if (lastQual == null) {
        System.out.println("Qualifier unknown:" + parts[0]);
        lastQual = new HashMap<String, String>();
        qualifiers.put(parts[0], lastQual);
      }
      lastQual.put("noValue", parts[1].equals("Y") ? "true" : "false");
      lastQual.put("new", parts[2].equals("Y") ? "true" : "false");
      lastQual.put("quoted", parts[3].equals("Y") ? "true" : "false");
      lastQual.put("regex", parts[4]);
      lastQual.put("fOrder", parts[5]);
      lastQual.put("commentParser", parts[6]);

      // NOVALUE NEW QUOTE REGEX FORDER COMMENT
    }
    allprops.add("noValue");
    allprops.add("new");
    allprops.add("quoted");
    allprops.add("regex");
    allprops.add("fOrder");
    allprops.add("commentParser");

    for (String item : allprops) {
      domain.createProperty("http://gbol.life#" + item);
      System.out.println(item);
    }
    domain.createProperty("http://gbol.life#ddbjLabel");

    RDFType objectProp = domain.createType("owl:ObjectProperty");
    for (String qualifier : qualifiers.keySet()) {
      RDFSubject rdfQualifer = objectProp.createObject("http://gbol.life#" + qualifier);
      rdfQualifer.addLit("http://gbol.life#ddbjLabel", qualifier);
      HashMap<String, String> props = qualifiers.get(qualifier);
      for (String prop : props.keySet()) {
        val = props.get(prop);
        if (prop == "noValue" || prop == "new" || prop == "quoted")
          rdfQualifer.add("http://gbol.life#" + prop, Boolean.parseBoolean(val));
        else if (prop == "fOrder")
          rdfQualifer.add("http://gbol.life#" + prop, Integer.parseInt(val));
        else
          rdfQualifer.addLit("http://gbol.life#" + prop, val);
      }
    }

    System.out.println("-------");

    qIn = FileUtils.readFileToString(new File("./datain/keys.txt")).split("\n");
    HashSet<String> allKeyProps = new HashSet<String>();
    HashMap<String, Object> lastFeature = null;
    HashMap<String, HashMap<String, Object>> featureKeys =
        new HashMap<String, HashMap<String, Object>>();
    lastKey = null;
    String lastKeyName = "";

    for (String line : qIn) {
      if (line.trim().equals(""))
        continue;
      if (line.startsWith(" ") || line.startsWith("\t")) {
        val += line.trim();
      } else {
        if (lastKey != null) {
          if (lastKey.endsWith("Qualifiers"))
            lastFeature.put(lastKey, parseKeySet(lastKeyName, val, qualifiers));
          else
            lastFeature.put(lastKey, val);
        }
        lastKey = line.split("[ \t]")[0];
        val = line.substring(lastKey.length()).trim();
        lastKey = lastKey.substring(0, 1).toLowerCase() + lastKey.substring(1);
        allKeyProps.add(lastKey);
        if (lastKey.equals("featureKey")) {
          lastFeature = new HashMap<String, Object>();
          featureKeys.put(val, lastFeature);
          lastKey = null;
          lastKeyName = val;
        }
      }
    }
    if (lastKey.endsWith("Qualifiers"))
      lastFeature.put(lastKey, parseKeySet(lastKeyName, val, qualifiers));
    else
      lastFeature.put(lastKey, val);

    for (String item : allKeyProps) {
      domain.createProperty("http://gbol.life#" + item, false);
      System.out.println(item);
    }


    qIn = FileUtils.readFileToString(new File("./validationRules/feature-keys.tsv")).split("\n");

    for (String line : qIn) {
      if (line.trim().length() == 0)
        continue;
      if (!featureKeys.containsKey(line.trim())) {
        System.out.println("Feature key not present: " + line);
      }
    }


    RDFType clazz = domain.createType("owl:Class");
    RDFType featureClazz = domain.createType("http://gbol.life#Feature");
    for (String feteatureKey : featureKeys.keySet()) {
      HashMap<String, Object> key = featureKeys.get(feteatureKey);
      // RDFSubject rdfQualifer = objectProp.createObject("http://gbol.life#" + qualifier);
      StmtIterator iter = con.listPatternLit(null, "http://gbol.life#ddbjLabel", feteatureKey);
      String featureRdfItem = null;
      if (iter.hasNext()) {
        featureRdfItem = iter.next().getSubject().getURI();
      }
      RDFSubject clazzObj = null;
      if (featureRdfItem == null) {
        featureRdfItem = "http://gbol.life#" + feteatureKey;
        domain.createType(featureRdfItem).addSubClassOf(featureClazz);
      }
      clazzObj = clazz.createObject(featureRdfItem);
      System.out.println(feteatureKey + "  " + featureRdfItem);
      clazzObj.addLit("http://gbol.life#ddbjLabel", feteatureKey);
      HashMap<String, Object> props = featureKeys.get(feteatureKey);
      for (String prop : props.keySet()) {
        Object oVal = props.get(prop);
        if (oVal instanceof HashSet) {
          for (String item : ((HashSet<String>) oVal))
            clazzObj.addLit("http://gbol.life#" + prop, item);
        } else
          clazzObj.addLit("http://gbol.life#" + prop, (String) oVal);
      }
    }

    con.save("gbolout.ttl");

  }
*/
  private Object parseKeySet(String key, String val,
      HashMap<String, HashMap<String, String>> qualifiers) {
    HashSet<String> toRet = new HashSet<String>();
    for (String item : val.split("/")) {
      // System.out.println(item);
      if (item.trim().length() == 0)
        continue;
      String parts[] = item.split("=");
      String qualKey = parts[0];
      String qualPattern = parts.length > 1 ? parts[1] : "<empty>";
      HashMap<String, String> qual = qualifiers.get(qualKey);
      if (qual == null) {
        System.out.println("" + key + ":" + qualKey + " -> not found");
        continue;
      }
      if (!qual.containsKey("keyPattern"))
        qual.put("keyPattern", qualPattern);
      else if (!qual.get("keyPattern").equals(qualPattern))
        System.out.println(
            "" + key + ":" + qualKey + " | " + qualPattern + " <-> " + qual.get("keyPattern"));
      toRet.add(qualKey);
    }
    return toRet;
  }
}
