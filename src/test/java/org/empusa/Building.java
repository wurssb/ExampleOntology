package org.empusa;

import junit.framework.TestCase;

/**
 * Created by jasperk on 03/04/2017.
 */

public class Building extends TestCase {

  public void testExample() throws Exception {
    String[] args = {"-i","example-ontology.ttl", "ExampleAdditional.ttl","-o",
        "../ExampleJavaApi","-r","../ExampleRApi","-owl","generated/Example.owl",
        "-ShExC","generated/Example.shexc","-rg","generated/Example_RDF2Graph.ttl"};
    Build.main(args);
  }
}
